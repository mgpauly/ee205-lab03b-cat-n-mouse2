#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

while true
do
   echo "OK cat, I'm thinking of a number from 1 to $THE_MAX_VALUE. Make a guess: "
   read guess 

   if [ $guess -lt 1 ]
   then
      echo "You must enter a number that's >= 1"
      continue
   fi

   if [ $guess -gt $THE_MAX_VALUE ]
   then
      echo "You must enter a number that's <= $THE_MAX_VALUE" 
      continue
   fi

   if [ $guess -gt $THE_NUMBER_IM_THINKING_OF ]
   then
      echo "No cat... the number I'm thinking of is smaller than $guess"
      continue
   fi

   if [ $guess -lt $THE_NUMBER_IM_THINKING_OF ]
   then
     echo "No cat... the number I'm thinking of is larger than $guess"
     continue
   fi

   if [ $guess -eq $THE_NUMBER_IM_THINKING_OF ]
   then
      break
   fi

done

echo You got me.
echo "|\---/|"
echo "| o_o |"
echo " \_^_/"
